package com.jannik_sundo.naskeeper.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jannik_sundo.naskeeper.R;
import com.jannik_sundo.naskeeper.adapters.SelectableAdapter;
import com.jannik_sundo.naskeeper.helpers.CommandHelper;
import com.jannik_sundo.naskeeper.models.Command;

import java.util.UUID;

public class AddEditCommandFragment extends Fragment {

    @SuppressWarnings("unused")
    private static final String TAG = AddEditCommandFragment.class.getSimpleName();
    private static final String ARG_DEVICE_ID = "command_id";
    private Command mCommand;

    public static AddEditCommandFragment newInstance(UUID commandId) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_DEVICE_ID, commandId);

        AddEditCommandFragment fragment = new AddEditCommandFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UUID commandId = (UUID) getArguments().getSerializable(ARG_DEVICE_ID);

        mCommand = CommandHelper.get(getActivity()).getCommand(commandId);
    }

    @Override
    public void onPause() {
        super.onPause();

        CommandHelper.get(getActivity())
                .updateCommand(mCommand);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_edit_command, container, false);
        EditText mNickname;
        EditText mIpAddress;
        EditText mCommandText;

        mNickname = (EditText) view.findViewById(R.id.command_nickname);
        mNickname.setText(mCommand.getNickname());
        mNickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCommand.setNickname(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mIpAddress = (EditText) view.findViewById(R.id.device_ip_address);
        mIpAddress.setText(mCommand.getIpAddress());
        mIpAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCommand.setIpAddress(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        mCommandText = (EditText) view.findViewById(R.id.command);
        mCommandText.setText(mCommand.getCommand());
        mCommandText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mCommand.setCommand(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        return view;
    }
}
