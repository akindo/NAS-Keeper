package com.jannik_sundo.naskeeper;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.jannik_sundo.naskeeper.fragments.AddEditCommandFragment;

import java.util.UUID;

public class AddEditCommandActivity extends SingleFragmentActivity {

    @SuppressWarnings("unused")
    private final String TAG = AddEditCommandActivity.class.getSimpleName();

    private static final String EXTRA_DEVICE_ID = "com.jannik_sundo.naskeeper.command_id";

    public static Intent newIntent(Context packageContext, UUID commandId) {
        Intent intent = new Intent(packageContext, AddEditCommandActivity.class);
        intent.putExtra(EXTRA_DEVICE_ID, commandId);

        return intent;
    }

    @Override
    protected Fragment createFragment() {
        Log.d(TAG, "Creating fragment with ID" + EXTRA_DEVICE_ID);
        UUID commandId = (UUID) getIntent().getSerializableExtra(EXTRA_DEVICE_ID);

        return AddEditCommandFragment.newInstance(commandId);
    }
}
