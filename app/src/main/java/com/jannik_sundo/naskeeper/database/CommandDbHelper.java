package com.jannik_sundo.naskeeper.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jannik_sundo.naskeeper.database.CommandDbSchema.CommandTable;

public class CommandDbHelper extends SQLiteOpenHelper {

    @SuppressWarnings("unused")
    private final String TAG = CommandDbHelper.class.getSimpleName();
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "commandDatabase.db";

    public CommandDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "Creating database.");

        db.execSQL("create table " + CommandDbSchema.CommandTable.NAME + "(" +
                        " _id integer primary key autoincrement, " +
                        CommandTable.Cols.UUID + ", " +
                        CommandTable.Cols.NICKNAME + ", " +
                        CommandTable.Cols.DEVICE_IP_ADDRESS + ", " +
                        CommandTable.Cols.DEVICE_MAC_ADDRESS + ", " +
                        CommandTable.Cols.COMMAND +
                        ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
