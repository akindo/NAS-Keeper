package com.jannik_sundo.naskeeper.database;

import com.jannik_sundo.naskeeper.adapters.SelectableAdapter;

public class CommandDbSchema {

    @SuppressWarnings("unused")
    private static final String TAG = SelectableAdapter.class.getSimpleName();

    public static final class CommandTable {
        public static final String NAME = "commands";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String NICKNAME = "nickname";
            public static final String DEVICE_IP_ADDRESS = "ip_address";
            public static final String DEVICE_MAC_ADDRESS = "mac_address";
            public static final String COMMAND = "command";
        }
    }
}
