package com.jannik_sundo.naskeeper.database;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.util.Log;

import com.jannik_sundo.naskeeper.models.Command;

import java.util.UUID;

import static com.jannik_sundo.naskeeper.database.CommandDbSchema.CommandTable;

public class CommandCursorWrapper extends CursorWrapper {

    @SuppressWarnings("unused")
    private final String TAG = CommandCursorWrapper.class.getSimpleName();

    public CommandCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    public Command getCommand() {
        String uuidString = getString(getColumnIndex(CommandTable.Cols.UUID));
        String nickname = getString(getColumnIndex(CommandTable.Cols.NICKNAME));
        String ipAddress = getString(getColumnIndex(CommandTable.Cols.DEVICE_IP_ADDRESS));
        String macAddress = getString(getColumnIndex(CommandTable.Cols.DEVICE_MAC_ADDRESS));
        String command = getString(getColumnIndex(CommandTable.Cols.COMMAND));

        Log.d(TAG, "Getting command with ID " + uuidString);
        Command mCommand = new Command(UUID.fromString(uuidString));
        mCommand.setNickname(nickname);
        mCommand.setIpAddress(ipAddress);
        mCommand.setMacAddress(macAddress);
        mCommand.setCommand(command);

        return mCommand;
    }
}
