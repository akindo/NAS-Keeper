package com.jannik_sundo.naskeeper;

import android.app.Application;
import android.os.StrictMode;

import com.facebook.stetho.Stetho;
import com.squareup.leakcanary.LeakCanary;

public class App extends Application {

    public static Application sContext;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;

        initDevHelpers();
    }

    public void initDevHelpers() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(
                                Stetho.defaultInspectorModulesProvider(this))
                        .build());

        LeakCanary.install(this);

        if (BuildConfig.DEVELOPER_MODE) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    //.detectDiskReads() // TODO: enable this once DB calls are on separate thread.
                    //.detectDiskWrites() // TODO: enable this once DB calls are on separate thread.
                    .detectNetwork() // or .detectAll() for all detectable problems.
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }
    }
}
