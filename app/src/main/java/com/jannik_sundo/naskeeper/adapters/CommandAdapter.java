package com.jannik_sundo.naskeeper.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jannik_sundo.naskeeper.AddEditCommandActivity;
import com.jannik_sundo.naskeeper.R;
import com.jannik_sundo.naskeeper.models.Command;

import java.util.List;

public class CommandAdapter extends SelectableAdapter<CommandAdapter.ViewHolder> {

    @SuppressWarnings("unused")
    private static final String TAG = CommandAdapter.class.getSimpleName();
    protected Context mContext;
    private List<Command> mCommands;
    private ViewHolder.ClickListener clickListener;

    public CommandAdapter(Context context, ViewHolder.ClickListener clickListener, List<Command> commands) {
        super();

        this.mContext = context;
        this.clickListener = clickListener;
        mCommands = commands;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_main, parent, false);
        return new ViewHolder(mContext, view, clickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Command command = mCommands.get(position);
        holder.bindCommand(command);

        // Highlight the item if it's selected
        holder.selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE :
                View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return mCommands.size();
    }

    public void setCommands(List<Command> commands) {
        mCommands = commands;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {

        @SuppressWarnings("unused")
        private static final String TAG = ViewHolder.class.getSimpleName();
        private Context context;
        private TextView mNicknameTextView;
        private TextView mIpAddressTextView;
        View selectedOverlay;
        private ClickListener listener;
        private Command mCommand;

        public ViewHolder(Context context, View itemView, ClickListener listener) {
            super(itemView);

            this.context = context;

            mNicknameTextView = (TextView) itemView.findViewById(R.id.list_commands_nickname_text_view);
            mIpAddressTextView =
                    (TextView) itemView.findViewById(R.id.list_commands_ipaddress_text_view);
            selectedOverlay = itemView.findViewById(R.id.selected_overlay);

            this.listener = listener;

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void bindCommand(Command command) {
            mCommand = command;
            mNicknameTextView.setText(mCommand.getNickname());
            mIpAddressTextView.setText(mCommand.getIpAddress());
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "Item clicked at position " + getPosition());

            if (listener != null) {
                listener.onItemClicked(getPosition());
            }

            Intent intent = AddEditCommandActivity.newIntent(context, mCommand.getId());
            context.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View v) {
            Log.d(TAG, "Item long-clicked at position " + getPosition());

            return listener == null || listener.onItemLongClicked(getPosition());

        }

        public interface ClickListener {
            void onItemClicked(int position);
            boolean onItemLongClicked(int position);
        }
    }
}