package com.jannik_sundo.naskeeper.models;

import java.util.UUID;

public class Command {

    @SuppressWarnings("unused")
    private static final String TAG = Command.class.getSimpleName();
    private UUID mId;
    private String mNickname;
    private String mIpAddress;
    private String mMacAddress;
    private String mCommand;

    public Command() {
        mId = UUID.randomUUID();
    }

    public Command(UUID id) {
        mId = id;
    }

    public UUID getId() {
        return mId;
    }

    public String getNickname() {
        return mNickname;
    }

    public void setNickname(String name) {
        mNickname = name;
    }

    public String getIpAddress() {
        return mIpAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.mIpAddress = ipAddress;
    }

    public String getMacAddress() {
        return mMacAddress;
    }

    public void setMacAddress(String macAddress) {
        this.mMacAddress = macAddress;
    }

    public String getCommand() {
        return mCommand;
    }

    public void setCommand(String command) {
        mCommand = command;
    }
}
