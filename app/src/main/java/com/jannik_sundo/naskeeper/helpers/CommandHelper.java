package com.jannik_sundo.naskeeper.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jannik_sundo.naskeeper.database.CommandDbHelper;
import com.jannik_sundo.naskeeper.database.CommandCursorWrapper;
import com.jannik_sundo.naskeeper.database.CommandDbSchema.CommandTable;
import com.jannik_sundo.naskeeper.models.Command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CommandHelper {

    @SuppressWarnings("unused")
    private static final String TAG = CommandHelper.class.getSimpleName();
    private static CommandHelper sCommandHelper;
    private SQLiteDatabase mDatabase;

    public static CommandHelper get(Context context) {
        if (sCommandHelper == null) {
            sCommandHelper = new CommandHelper(context);
        }

        return sCommandHelper;
    }

    private CommandHelper(Context context) {
        Context mContext = context.getApplicationContext();
        mDatabase = new CommandDbHelper(mContext).getWritableDatabase();
    }

    public void addCommand(Command command) {
        ContentValues values = getContentValues(command);
        mDatabase.insert(CommandTable.NAME, null, values);
    }

    public List<Command> getCommands() {
        List<Command> commands = new ArrayList<>();

        CommandCursorWrapper cursor = queryCommands(null, null);

        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                commands.add(cursor.getCommand());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return commands;
    }

    public Command getCommand(UUID id) {
        CommandCursorWrapper cursor = queryCommands(
                CommandTable.Cols.UUID + " = ?",
                new String[] { id.toString() }
        );

        try {
            if (cursor.getCount() == 0) {
                return null;
            }

            cursor.moveToFirst();
            return cursor.getCommand();
        } finally {
            cursor.close();
        }
    }

    public void updateCommand(Command command) {
        String uuidString = command.getId().toString();
        ContentValues values = getContentValues(command);

        mDatabase.update(CommandTable.NAME, values, CommandTable.Cols.UUID + " = ?",
                new String[] {uuidString});
    }

    private static ContentValues getContentValues(Command command) {
        ContentValues values = new ContentValues();
        values.put(CommandTable.Cols.UUID, command.getId().toString());
        values.put(CommandTable.Cols.DEVICE_IP_ADDRESS, command.getIpAddress());
        values.put(CommandTable.Cols.DEVICE_MAC_ADDRESS, command.getMacAddress());
        values.put(CommandTable.Cols.COMMAND, command.getCommand());

        return values;
    }

    private CommandCursorWrapper queryCommands(String whereClause, String[] whereArgs) {
        Cursor cursor = mDatabase.query(
                CommandTable.NAME,
                null, // Columns - null selects all columns
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null  // orderBy
        );

        CommandCursorWrapper commandCursorWrapper = new CommandCursorWrapper(cursor);
        // TODO: can't close cursor when inside the wrapper. Does it get closed when closing the
        // wrapper?
        //cursor.close();

        return commandCursorWrapper;
    }
}
