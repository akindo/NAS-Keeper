package com.jannik_sundo.naskeeper;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.jannik_sundo.naskeeper.fragments.MainFragment;
import com.jannik_sundo.naskeeper.helpers.CommandHelper;
import com.jannik_sundo.naskeeper.models.Command;

public class MainActivity extends SingleFragmentActivity {

    @SuppressWarnings("unused")
    private final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected Fragment createFragment() {
        return new MainFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Command command = new Command();
                CommandHelper.get(getApplicationContext()).addCommand(command);
                Intent intent = AddEditCommandActivity.newIntent(getApplicationContext(), command.getId());
                startActivity(intent);
            }
        });
    }
}
